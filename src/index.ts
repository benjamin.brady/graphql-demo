import { ApolloServer } from 'apollo-server-express';
import { Bid, Member, PrismaClient } from "@prisma/client";
import DataLoader from "dataloader"
import fs from "fs"
import path from "path"
import { PubSub } from 'graphql-subscriptions';
import { ApolloServerPluginDrainHttpServer } from 'apollo-server-core';
import express from 'express';
import http from 'http';
import { execute, subscribe } from 'graphql';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import { makeExecutableSchema } from '@graphql-tools/schema';

const pubsub = new PubSub();

const prisma = new PrismaClient({
    log: ['query', 'info', 'warn', 'error'],
})

var bidsLoader = new DataLoader<number, Bid[]>(async (listingIds) => {
    var ids = [...listingIds]
    let bids = await prisma.bid.findMany({
        where: {
            listingId: {
                in: ids
            }
        }
    });
    let bidsByListingId: { [key: number] : Bid[] }  = {};
    bids.forEach(bid => {
        let bids = bidsByListingId[bid.listingId] ?? []
        bids.push(bid)
        bidsByListingId[bid.listingId] = bids
    });
    return listingIds.map(listingId => bidsByListingId[listingId])
});

var memberLoader = new DataLoader<number, Member>(async (memberIds) => {
    var ids = [...memberIds]
    let members = await prisma.member.findMany({
        where: {
            memberId: {
                in: ids
            }
        }
    });
    let membersByMemberId: { [key: number] : Member }  = {};
    members.forEach(member => {
        membersByMemberId[member.memberId] = member
    });
    return members.map(member => membersByMemberId[member.memberId])
});

async function getMember(memberId: number){
    var member =  await prisma.member.findFirst({
        where: {
            memberId: memberId
        }
    });
    return member;
}

async function getListingBids(listingId: number) {
    let bids = await prisma.bid.findMany({
        where: {
            listingId: listingId
        },
        orderBy: {
            createdAt: "desc"
        },
        take: 10
    });
    return bids;
} 

const useBatchedQueries = true;
// Resolvers define the technique for fetching the types defined in the schema. 
const resolvers = {
    Listing: {
        bids: async (parent: any, args: any, context: any, info: any) => {
            if(useBatchedQueries) {
                return await bidsLoader.load(parent.listingId) ?? []
            } else {
                return await getListingBids(parent.listingId); // slow - many queries
            }
        }
    },
    Bid: {
        member: async (parent: any, args: any, context: any, info: any) => {
            if(useBatchedQueries) {
                return await memberLoader.load(parent.memberId) 
            } else {
                return await getMember(parent.memberId) // slow - many queries
            }    
        }
    },
    Query: {
      listings: async (parent: any, args: any, context: any, info: any) => {
        return await prisma.listing.findMany();
      },
      members: async (parent: any, args: any, context: any, info: any) => {
        return await prisma.member.findMany();
      }
    },
    Mutation: {
        createListing: async (parent: any, args: any) => {
            const newListing = prisma.listing.create({
                data: {
                    title: args.title,
                    description: args.description,
                    memberId: args.memberId
                }
            });
            console.log(newListing);
            return newListing;
        },
        createBid: async (parent: any, args: any, context: any, info: any) => {
            const newBid = await prisma.bid.create({
                data: {
                    memberId: args.memberId,
                    listingId: args.listingId,
                    price: args.price
                }
            });
            pubsub.publish(`NEW_BID-listingId:${newBid.listingId}]`, newBid);
            return newBid;
        }
    },
    Subscription: {
        newBid: {
            subscribe: async (parent: any, args: any, context: any, info: any) => { 
                return pubsub.asyncIterator(`NEW_BID-listingId:${args.listingId}]`)
            },
            resolve: (payload: Bid) => payload
        }
    }
  };
  

async function startApolloServer(typeDefs: any, resolvers: any) {
    // Required logic for integrating with Express
    const app = express();
    const httpServer = http.createServer(app);
    const schema = makeExecutableSchema({ typeDefs, resolvers });

    const subscriptionServer = SubscriptionServer.create({
        // This is the `schema` we just created.
        schema,
        // These are imported from `graphql`.
        execute,
        subscribe,
     }, {
        // This is the `httpServer` we created in a previous step.
        server: httpServer,
        // Pass a different path here if your ApolloServer serves at
        // a different path.
        path: '/',
     });

    // Same ApolloServer initialization as before, plus the drain plugin.
    const server = new ApolloServer({
        schema,
        plugins: [
            ApolloServerPluginDrainHttpServer({ httpServer }),
            {
                async serverWillStart() {
                  return {
                    async drainServer() {
                      subscriptionServer.close();
                    }
                  };
                }
            }
        ],
    });

    // More required logic for integrating with Express
    await server.start();
    server.applyMiddleware({
        app,

        // By default, apollo-server hosts its GraphQL endpoint at the
        // server root. However, *other* Apollo Server packages host it at
        // /graphql. Optionally provide this to match apollo-server.
        path: '/'
    });

    // Modified server startup
    await new Promise<void>(resolve => httpServer.listen({ port: 4000 }, resolve));
    console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`);
}

var typeDefs = fs.readFileSync(
            path.join(__dirname, 'schema.graphql'),
            'utf8' )

startApolloServer(typeDefs, resolvers);