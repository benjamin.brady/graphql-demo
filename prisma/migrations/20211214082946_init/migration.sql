/*
  Warnings:

  - You are about to drop the `_ListingToMember` table. If the table is not empty, all the data it contains will be lost.
  - Added the required column `memberId` to the `Listing` table without a default value. This is not possible if the table is not empty.
  - Added the required column `memberId` to the `Bid` table without a default value. This is not possible if the table is not empty.

*/
-- DropIndex
DROP INDEX "_ListingToMember_B_index";

-- DropIndex
DROP INDEX "_ListingToMember_AB_unique";

-- DropTable
PRAGMA foreign_keys=off;
DROP TABLE "_ListingToMember";
PRAGMA foreign_keys=on;

-- CreateTable
CREATE TABLE "Watch" (
    "watchId" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "memberId" INTEGER NOT NULL,
    "listingId" INTEGER NOT NULL,
    CONSTRAINT "Watch_memberId_fkey" FOREIGN KEY ("memberId") REFERENCES "Member" ("memberId") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Watch_listingId_fkey" FOREIGN KEY ("listingId") REFERENCES "Listing" ("listingId") ON DELETE RESTRICT ON UPDATE CASCADE
);

-- RedefineTables
PRAGMA foreign_keys=OFF;
CREATE TABLE "new_Listing" (
    "listingId" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "memberId" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "title" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    CONSTRAINT "Listing_memberId_fkey" FOREIGN KEY ("memberId") REFERENCES "Member" ("memberId") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Listing" ("createdAt", "description", "listingId", "title") SELECT "createdAt", "description", "listingId", "title" FROM "Listing";
DROP TABLE "Listing";
ALTER TABLE "new_Listing" RENAME TO "Listing";
CREATE TABLE "new_Bid" (
    "bidId" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "memberId" INTEGER NOT NULL,
    "listingId" INTEGER NOT NULL,
    "createdAt" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "price" DECIMAL NOT NULL,
    CONSTRAINT "Bid_memberId_fkey" FOREIGN KEY ("memberId") REFERENCES "Member" ("memberId") ON DELETE RESTRICT ON UPDATE CASCADE,
    CONSTRAINT "Bid_listingId_fkey" FOREIGN KEY ("listingId") REFERENCES "Listing" ("listingId") ON DELETE RESTRICT ON UPDATE CASCADE
);
INSERT INTO "new_Bid" ("bidId", "createdAt", "listingId", "price") SELECT "bidId", "createdAt", "listingId", "price" FROM "Bid";
DROP TABLE "Bid";
ALTER TABLE "new_Bid" RENAME TO "Bid";
PRAGMA foreign_key_check;
PRAGMA foreign_keys=ON;
