# GraphQL Demo

This is the GraphQL server written in typescript for a fictional 'YouBid' auction company.

## Getting started 

You must have nodejs/npm installed.

Install node pacakges:

    npm install

Create and update local SQLite database:

    npx prisma migrate dev

Run locally:

    npm run dev

Navigate to http://localhost:4000/

## Example

### Query

```
query {
  listings {
    title
    description
    bids {
      price
      member {
        memberId
        name
        email
      }
    }
  }
}
```

## Inspect Database 

Runs Prisma studio, a browser  based SQL client.

    npx prisma studio
